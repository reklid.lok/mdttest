<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            [
                'email' => 'admin@admin.com',
                'name' => 'Админ',
                'password' => '$2y$10$BnTwgyCo3evMk7u7Fvks7.JRB84G6q1sEUHC3FXaz/lK6NKmNv3mm', // 123456
                'created_at' => date('Y-m-d'),
                'email_verified_at' => date('Y-m-d'),
            ],
        ]);
    }
}
