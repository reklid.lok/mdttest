@extends('layouts.app')

@section('title', 'Подтверждение Email')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8" style="margin-top: 2%">
                <div class="card" style="width: 40rem;">
                    <div class="card-body">
                        <h4 class="card-title">Подтверждение Email</h4>
                        @if (session('resent'))
                            <p class="alert alert-success" role="alert">
                                Ссылка для подтверждения выслана вам на Email.
                            </p>
                        @endif
                        <p class="card-text">
                            Подтвердите свой Email, прежде чем пользоваться сайтом.
                        </p>
                        <p class="card-text">
                            Если вам ничего не пришло на почту, <a href="{{ route('verify.resend') }}">кликните сюда для отправки повторного письма</a>.
                        </p>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
