<?php

return [

    'add_new'      => 'Добавить',
    'cancel'       => 'Отмена',
    'save'         => 'Сохранить',
    'edit'         => 'Редактировать',
    'detail'       => 'Детально',
    'back'         => 'Назад',
    'action'       => 'Действия',
    'id'           => 'ID',
    'created_at'   => 'Создано',
    'updated_at'   => 'Обновлено',
    'deleted_at'   => 'Удалено',
    'are_you_sure' => 'Вы уверены?',
];
