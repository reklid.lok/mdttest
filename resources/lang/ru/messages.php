<?php

return [

    'retrieved' => ':model успешно восстановлено.',
    'saved'     => ':model успешно сохранено.',
    'updated'   => ':model успешно обновлено.',
    'deleted'   => ':model успешно удалено.',
    'not_found' => ':model не найдено.',

];
