<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed'   => 'Неверный email или пароль.',
    'throttle' => 'Слишком много попыток входа в систему. Повторите попытку через :seconds секунды.',

    'full_name'        => 'Имя',
    'surname'          => 'Фамилия',
    'patronymic'       => 'Отчество',
    'birthday'         => 'Дата рождения',
    'phone'            => 'Номер телефона',
    'email'            => 'Email',
    'password'         => 'Пароль',
    'confirm_password' => 'Подтверждение пароля',
    'remember_me'      => 'Запомнить меня',
    'sign_in'          => 'Войти',
    'sign_out'         => 'Выйти',
    'register'         => 'Регистрация',

    'login' => [
        'title'               => 'Войдите для начала работы',
        'forgot_password'     => 'Забыли пароль?',
        'register_membership' => 'Регистрация нового пользователя',
    ],

    'registration' => [
        'title'           => 'Регистрация',
        'i_agree'         => 'Я согласен с',
        'terms'           => 'правилами',
        'have_membership' => 'Уже есть аккаунт',
    ],

    'forgot_password' => [
        'title'          => 'Введите Email для сброса пароля',
        'send_pwd_reset' => 'Отправить ссылку',
    ],

    'reset_password' => [
        'title'         => 'Сбросить пароль',
        'reset_pwd_btn' => 'Сбросить пароль',
    ],

    'emails' => [
        'password' => [
            'reset_link' => 'Кликните здесь для сброса пароля',
        ],
    ],

    'app' => [
        'member_since' => 'Пользователь начиная с',
        'messages'     => 'Сообщения',
        'settings'     => 'Настройки',
        'lock_account' => 'Заблокировать аккаунт',
        'profile'      => 'Профиль',
        'online'       => 'Онлайн',
        'search'       => 'Поиск',
        'create'       => 'Создать',
        'export'       => 'Экспорт',
        'print'        => 'Печать',
        'reset'        => 'Сбросить',
        'reload'       => 'Перезагрузить',
    ],
];
